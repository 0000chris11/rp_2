//22) �La langosta ahumada� es una empresa dedicada a ofrecer banquetes; sus tarifas son las siguientes: el costo de platillo por persona es de $9500, 
//pero si el n�mero de personas es mayor a 200 pero menor o igual a 300, el costo es de $8500. Para m�s de 300 personas el costo por platillo es de $7500. 
//Se requiere un algoritmo que ayude a determinar el presupuesto que se debe presentar a los clientes que deseen realizar un evento. Mediante pseudoc�digo, diagrama de flujo.
Funcion RS <- personas_platillo(persona)
	si persona>200 Y persona<=300
		RS = 8500
	SiNo
		Si persona>300
			RS = 7500
		FinSi
		si persona<200
			RS = 9500
		FinSi
	FinSi
FinFuncion

Algoritmo sin_titulo
	Escribir "Numero de Personas: "
	Leer persona
	
	Escribir "Costo por Platillo: " personas_platillo(persona)
FinAlgoritmo
