//23.- La asociaci�n de vinicultores tiene como pol�tica fijar un precio inicial al kilo de uva, la cual se clasifica en tipos A y B, y adem�s en tama�os 1 y 2//. 
//Cuando se realiza la venta del producto, �sta es de un solo tipo y tama�o, se requiere determinar cu�nto recibir� un productor por la uva que entrega en un embarque, 
//considerando lo siguiente: si es de tipo A, se le cargan $20 al precio inicial cuando es de tama�o 1; y $30 si es de tama�o 2. Si es de tipo B, 
//se rebajan $30 cuando es de tama�o 1, y $50 cuando es de tama�o 2. Realice un algoritmo para determinar la ganancia obtenida y repres�ntelo mediante diagrama de flujo y pseudoc�digo.   
Funcion RS <- tipo_tama�o(tip,tam,prec)
	Si tip == "A"
		Si tam == 1
			RS = prec + 20
		FinSi
		Si tam == 2
			RS = prec + 30
		FinSi
	FinSi
	Si tip == "B"
		Si tam == 1
			RS = prec - 30
		FinSi
		Si tam == 2
			RS = prec - 50
		FinSi
		
	FinSi
FinFuncion

Algoritmo sin_titulo
	Escribir "Tipo de Uva: "
	Leer tip
	Escribir "Tama�o de Uva: "
	Leer tam
	Escribir "Precio Inicial: "
	Leer prec
	
	Escribir "Precio de kilo de Uva: " tipo_tama�o(tip,tam,prec)
	

FinAlgoritmo
