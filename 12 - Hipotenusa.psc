//12)Se requiere determinar la hipotenusa de untriangulo rectangulo. Recuerde que por Pitagoras se tiene que: C2=A2+B2
Funcion Resultado <- cateto_cateto(A,B)
	Resultado = (A*A)+(B*B)
FinFuncion

Algoritmo Hipotenusa
	Escribir "Cateto 1: "
	Leer A
	Escribir "Cateto 2: "
	Leer B
	
	Escribir "Hipotenusa: " cateto_cateto(A,B)
FinAlgoritmo
