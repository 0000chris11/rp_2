//20) Almacenes �El harapiento distinguido� tiene una promoci�n: a todos los trajes que tienen un precio superior a $25000 se les aplicar� un descuento de 15 %, 
//a todos los dem�s se les aplicar� s�lo 8 %. Realice un algoritmo para determinar el precio final que debe pagar una persona por comprar un traje y de cu�nto es el descuento que obtendr�. 
//Repres�ntelo mediante el pseudoc�digo, el diagrama de flujo
Funcion RS <- traje_precio(traje,desc)
	RS = traje-desc
FinFuncion

Algoritmo sin_titulo
	Escribir "Precio del Traje: "
	Leer traje
	si (traje>25000)
			desc = traje*0.15
		SiNo
			desc = traje*0.08
		FinSi
	
		Escribir "Precio final: " traje_precio(traje,desc)
		Escribir "Descuento: " desc
FinAlgoritmo
