//25.- Realice el diagrama de flujo, el pseudoc�digo y que muestren el algoritmo para determinar el costo y el descuento que tendr� un art�culo. 
//Considere que si su precio es mayor o igual a $200 se le aplica un descuento de 15%, y si su precio es mayor a $100 pero menor a $200, 
//el descuento es de 12%, y si es menor a $100, s�lo 10%.
Funcion RS <- precio_descuento(prec)
	Si (prec>=200)
		desc = prec * 0.15
		RS = prec-desc
	FinSi
	Si prec > 100 Y prec < 200
		desc = prec * 0.12
		RS = prec-desc
	FinSi
	Si prec < 100
		desc = prec * 0.10
		RS = prec-desc
	FinSi
FinFuncion

Algoritmo sin_titulo
	Escribir "Precio Inicial: "
	Leer prec
	
	Escribir "Precio Final: " precio_descuento(prec)
	
FinAlgoritmo
