//11) Pinturas "La brocha gorada" requiere determinar cuanto cobrar por trabajos de pintura. 
//Considere que se cobra por m2.Realize un DF y PSC que le permita ir generando presupuestos para cada cliente
Funcion Resultado <- cobro_metro(C,M)
	Resultado = (M*M)*C
FinFuncion

Algoritmo Cobro_por_m2
	Escribir "Cobro: "
	Leer C
	Escribir "metros: "
	Leer M
	
	Escribir "Cobro por metro cuadrado:" cobro_metro(C,M)
FinAlgoritmo
