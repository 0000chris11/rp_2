//19) Realice un algoritmo para determinar cu�nto se debe pagar por equis cantidad de l�pices considerando que si son 1000 o m�s el costo es de $85; de lo contrario, 
//el precio es de $90. Repres�ntelo con el pseudoc�digo, el diagrama de flujo.
Funcion RS <- lapices_precio(lapic)
	Si (lapic>1000)
		RS = 85
	SiNo
		RS = 90
	FinSi
FinFuncion

Algoritmo sin_titulo
	Escribir "Cantidad de Lapices: "
	Leer lapic
	
	Escribir "Precio: " lapices_precio(lapic)
FinAlgoritmo
